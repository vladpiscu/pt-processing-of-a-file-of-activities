package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
	private Date startTime;
	private Date endTime;
	private String activityLabel;
	
	public MonitoredData(String startTime, String endTime, String activityLabel)
	{
		try{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			this.startTime = df.parse(startTime);
			this.endTime = df.parse(endTime);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		this.activityLabel = new String(activityLabel);
	}
	
	public Date getStartTime()
	{
		return startTime;
	}
	
	public Date getEndTime()
	{
		return endTime;
	}
	
	public String getActivityLabel()
	{
		return activityLabel;
	}
	
	public String toString()
	{
		return startTime.toString() + " " + endTime.toString() + " " + activityLabel;
	}
}
