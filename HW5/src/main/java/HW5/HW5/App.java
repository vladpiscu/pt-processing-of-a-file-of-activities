package HW5.HW5;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import model.*;


public class App 
{
	
    @SuppressWarnings("deprecation")
	public static void main( String[] args )
    {
    	List<MonitoredData> d = new ArrayList<MonitoredData>();
    	try {
			Scanner in = new Scanner(new FileReader("activities.txt"));
			while(in.hasNextLine())
			{
				String s = in.nextLine();
				StringTokenizer t = new StringTokenizer(s);
				String startDate = t.nextToken()+ " " + t.nextToken();
				String endDate = t.nextToken() + " " + t.nextToken();
				d.add(new MonitoredData(startDate, endDate, t.nextToken()));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	System.out.println(d.stream().map(d1 -> d1.getStartTime().getDate()).distinct().count());
    	pointTwo(d);
    	pointThree(d);
    	pointFour(d);
    	pointFive(d);
    }
    
    public static void pointTwo(List<MonitoredData> d)
    {
    	Map<Object, Object> map = d.stream()
    	    	.map(md ->
    	    	md.getActivityLabel())
    	    	.distinct()
    	    	.collect(Collectors.toMap(md -> md, md -> d.stream().filter(d1 -> d1.getActivityLabel().equalsIgnoreCase(md)).count()));
    	    	List<String> activities = d.stream()
    	    	.map(md ->
    	    	md.getActivityLabel())
    	    	.distinct()
    	    	.collect(Collectors.toList());
    	
    	try{
		    PrintWriter writer = new PrintWriter("point2.txt", "UTF-8");
		    for(String a : activities)
	       	{
	    		writer.println("Activity: " + a + " - number of occurences: " + map.get(a));
	    	}
		    writer.close();
		} catch (IOException e) {
		}
    }
    
    public static void pointThree(List<MonitoredData> d)
    {
    	Map<Object, Map<Object, Object>> map = 
    			d.stream()
    			.map(md -> md.getStartTime().getDate())
    			.distinct()
    			.collect(Collectors.toMap(day -> day, 
    					day -> d.stream()
    					.filter(md -> md.getStartTime().getDate() == day)
    	    	    	.map(md ->
    	    	    	md.getActivityLabel())
    	    	    	.distinct()
    	    	    	.collect(Collectors.toMap(md -> md, 
    	    	    			md -> d.stream()
    	    	    			.filter(d1 -> d1.getActivityLabel().equalsIgnoreCase(md) && d1.getStartTime().getDate() == day)
    	    	    			.count()))
    					));
    	
    	List<Integer> dates = d.stream()
    			.map(md -> md.getStartTime().getDate())
    			.distinct()
    			.collect(Collectors.toList());
    	
    	List<String> activities = d.stream()
    	    	.map(md ->
    	    	md.getActivityLabel())
    	    	.distinct()
    	    	.collect(Collectors.toList());
    	try{
		    PrintWriter writer = new PrintWriter("point3.txt", "UTF-8");
		    for(Integer date : dates)
	    	{
	    		for(String s : activities)
	    		{
	    			writer.println("Date: " + date + " - Activity: " + s + " - Occurences: " + map.get(date).get(s));
	    		}
	    	}
		    writer.close();
		} catch (IOException e) {
		}
    		
    }
    
    public static void pointFour(List<MonitoredData> d)
    {
    	Map<Object, Object> map = d.stream()
    	    	.map(md ->
    	    	md.getActivityLabel())
    	    	.distinct()
    	    	.collect(Collectors.toMap(md -> md, md -> d.stream()
    	    				.filter(d1 -> d1.getActivityLabel().equalsIgnoreCase(md))
    	    				.mapToLong(d1 -> computeDateDifference(d1.getStartTime(), d1.getEndTime(), TimeUnit.SECONDS))
    	    				.sum()));
    	Map<Object, Object> finalMap = map.keySet().stream().filter(entry -> (Long) map.get(entry) >= 36000)
    			.collect(Collectors.toMap(entry -> entry, entry -> (Long) map.get(entry) / 3600));
    	List<String> activities = d.stream()
    	    	.map(md ->
    	    	md.getActivityLabel())
    	    	.distinct()
    	    	.collect(Collectors.toList());
    	
    	try{
		    PrintWriter writer = new PrintWriter("point4.txt", "UTF-8");
		    for(String a : activities)
	       	{
	    	  	if(finalMap.get(a) != null)
	    	  		writer.println("Activity: " + a + " - total duration: " + finalMap.get(a));
	    	}
		    writer.close();
		} catch (IOException e) {
		}
    	    	
    }
    
    public static void pointFive(List<MonitoredData> d)
    {
    	List<String> activities = d.stream()
    			.filter(md ->  9 *
    				d.stream().filter(md1 -> md1.getActivityLabel().equalsIgnoreCase(md.getActivityLabel())).count() / 10 <=
    				d.stream().filter(md1 -> md1.getActivityLabel().equalsIgnoreCase(md.getActivityLabel()) 
    				&& computeDateDifference(md1.getStartTime(), md1.getEndTime(), TimeUnit.MINUTES) < 5).count()
    				).map(md -> md.getActivityLabel()).distinct().collect(Collectors.toList());
    	try{
		    PrintWriter writer = new PrintWriter("point5.txt", "UTF-8");
		    for(String a : activities)
	       	{
	    	  	writer.println("Activity: " + a);
	    	}
		    writer.close();
		} catch (IOException e) {
		}
    }
    
    public static long computeDateDifference(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }
    
    
    
    
}
